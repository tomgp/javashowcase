package com.tomgpdev.models.impl;

import com.tomgpdev.models.Car;
import com.tomgpdev.models.Tire;
import com.tomgpdev.servicelocator.ServiceLocator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class CarImpl implements Car {
    private static final Log log = LogFactory.getLog(CarImpl.class);

    private static final int AMOUNT_OF_TIRES = 4;
    private List<Tire> tires = new ArrayList<>();

    public CarImpl() {
        for (int i = 1; i <= AMOUNT_OF_TIRES; ++i) {
            tires.add(generateTire(i));
        }
    }

    @Override
    public void startEngine() {
        log.debug("Brooom!!! Brooom!!!!");
        tires.forEach(Tire::roll);
    }

    private Tire generateTire(int position) {
        Tire tire = ServiceLocator.get(Tire.class);
        tire.setPosition(position);

        return tire;
    }
}
