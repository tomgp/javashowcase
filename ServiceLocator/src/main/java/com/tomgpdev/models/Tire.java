package com.tomgpdev.models;

public interface Tire {
    void setPosition(int pos);
    void roll();
}
