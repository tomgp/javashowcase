package com.tomgpdev.factories.cardependencies;

import com.tomgpdev.models.Tire;
import com.tomgpdev.models.impl.TireImpl;
import com.tomgpdev.servicelocator.ServiceLocatorFactory;

@SuppressWarnings("unchecked")
public class TireFactory extends ServiceLocatorFactory<Tire> {

    @Override
    public Tire get() {
        return new TireImpl();
    }

    @Override
    public Class classToRegister() {
        return Tire.class;
    }
}
