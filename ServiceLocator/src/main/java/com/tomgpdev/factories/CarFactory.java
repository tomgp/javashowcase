package com.tomgpdev.factories;

import com.tomgpdev.models.Car;
import com.tomgpdev.models.impl.CarImpl;
import com.tomgpdev.servicelocator.ServiceLocatorFactory;

@SuppressWarnings("ALL")
public class CarFactory extends ServiceLocatorFactory<Car> {

    @Override
    public Car get() {
        return new CarImpl();
    }

    @Override
    public Class classToRegister() {
        return Car.class;
    }
}
