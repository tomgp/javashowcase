package com.tomgpdev;

import com.tomgpdev.models.Car;
import com.tomgpdev.servicelocator.ServiceLocator;

public class Main {

    public static void main(String[] args) {
        Main main = new Main();
        main.letsDoSomething();
    }

    public void letsDoSomething() {
        Car car = ServiceLocator.get(Car.class);
        car.startEngine();
    }
}
