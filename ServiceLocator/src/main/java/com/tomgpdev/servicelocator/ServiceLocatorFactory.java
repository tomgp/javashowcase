package com.tomgpdev.servicelocator;

public abstract class ServiceLocatorFactory<T> {
    public abstract T get();
    public abstract Class<T> classToRegister();
}
