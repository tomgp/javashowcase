package com.tomgpdev.servicelocator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.net.URL;
import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public final class InternalServiceLocatorImpl implements InternalServiceLocator {
    private static final Log log = LogFactory.getLog(InternalServiceLocatorImpl.class);

    public static final String DEFAULT_PACKAGE_TO_LOOK_FOR_SERVICE_LOCATOR_FACTORIES = "com.tomgpdev.factories";

    private final Map<Class, ServiceLocatorFactory> factories = new HashMap<>();
    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    private String lookForFactoriesInPackage = DEFAULT_PACKAGE_TO_LOOK_FOR_SERVICE_LOCATOR_FACTORIES;

    public <T> T get(Class<T> classToInstantiate) {
        Objects.requireNonNull(classToInstantiate);

        lock.readLock().lock();

        initTakingCareOfLocks();

        ServiceLocatorFactory objectLocatorFactory = factories.get(classToInstantiate);

        lock.readLock().unlock();

        Objects.requireNonNull(objectLocatorFactory);
        return (T) objectLocatorFactory.get();
    }

    public void setFactories(Map<Class, ServiceLocatorFactory> factories) {
        lock.writeLock().lock();

        this.factories.clear();
        this.factories.putAll(factories);

        lock.writeLock().unlock();
    }

    @Override
    public Map<Class, ServiceLocatorFactory> getFactories() {
        try {
            lock.readLock().lock();

            initTakingCareOfLocks();

            return factories;
        } finally {
            lock.readLock().unlock();
        }
    }

    private void initTakingCareOfLocks() {
        if (factories.isEmpty()) {
            lock.readLock().unlock();

            lock.writeLock().lock();
            init();
            lock.writeLock().unlock();

            lock.readLock().lock();
        }
    }

    @Override
    public void lookForFactoriesIn(String packageName) {
        this.lookForFactoriesInPackage = packageName;
    }

    private void init() {
        if (!factories.isEmpty()) {
            return;
        }

        for (String className : getClassNames()) {
            ServiceLocatorFactory factory = null;

            try {
                factory = (ServiceLocatorFactory) Class.forName(className).newInstance();
            } catch (ClassCastException | IllegalAccessException | ClassNotFoundException | InstantiationException e) {
                log.debug("We couldn't instantiate as an Object Locator factory this class: " + className);
            }

            if (factory != null) {
                factories.put(factory.classToRegister(), factory);
            }
        }

        logFactories();
    }

    private void logFactories() {
        String result;

        if (!factories.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (ServiceLocatorFactory value : factories.values()) {
                sb.append(value.getClass().getCanonicalName());
                sb.append(",");
            }

            String factories = sb.toString();
            if (factories.lastIndexOf(",") == (factories.length() - 1)) {
                factories = factories.substring(0, factories.length() - 1);
            }

            result = String.format("Instantiated %1$d factories: %2$s", this.factories.size(), factories);
        } else {
            result = "No factories found.";
        }

        log.debug(result);
    }

    private List<String> getClassNames() {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        String packagePath = lookForFactoriesInPackage.replaceAll("\\.", File.separator);
        URL packageUrl = classLoader.getResource(packagePath);
        if (packageUrl == null) {
            throw new IllegalStateException("Package not found at: " + packagePath);
        }

        switch (packageUrl.getProtocol()) {
            case "file":
                return getClassPathsFromDirectory(packageUrl.toString().substring("file:".length()), lookForFactoriesInPackage);
            default:
                // Classes could be in an jar... not exploded in the file system like in the case of deploying .war files,
                // so it would be needed an implementation for those
                throw new IllegalStateException("URL Protocol not implemented");
        }
    }

    private List<String> getClassPathsFromDirectory(String packagePath, String packageName) {
        List<String> result = new ArrayList<>();

        File dir = new File(packagePath);
        if (!dir.exists()) {
            throw new IllegalArgumentException("Package path directory doesn't exist: " + packagePath);
        }

        final String classSuffix = ".class";

        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    result.addAll(getClassPathsFromDirectory(file.getPath(), packageName + "." + file.getName()));
                } else {
                    String path = file.getPath();

                    if (path.endsWith(classSuffix)) {
                        path = packageName + "." + path.substring(path.lastIndexOf(File.separator) + 1, path.length() - classSuffix.length());
                        result.add(path);
                    }
                }
            }
        }

        return result;
    }
}
