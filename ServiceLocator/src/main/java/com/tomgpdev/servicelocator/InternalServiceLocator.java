package com.tomgpdev.servicelocator;

import java.util.Map;

public interface InternalServiceLocator {
    <T> T get(Class<T> classToInstantiate);
    void setFactories(Map<Class, ServiceLocatorFactory> factories);
    Map<Class, ServiceLocatorFactory> getFactories();
    void lookForFactoriesIn(String packageName);
}
