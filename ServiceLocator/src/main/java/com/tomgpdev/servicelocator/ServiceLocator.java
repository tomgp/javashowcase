package com.tomgpdev.servicelocator;

import java.util.Map;

public final class ServiceLocator {

    private static InternalServiceLocator internalServiceLocator = new InternalServiceLocatorImpl();

    public static <T> T get(Class<T> classToInstantiate) {
        return internalServiceLocator.get(classToInstantiate);
    }

    public static void setFactories(Map<Class, ServiceLocatorFactory> factories) {
        internalServiceLocator.setFactories(factories);
    }

    public static void changeInternalServiceLocator(InternalServiceLocator internalServiceLocator) {
        ServiceLocator.internalServiceLocator = internalServiceLocator;
    }

    public static void lookForFactoriesIn(String packageName) {
        ServiceLocator.internalServiceLocator.lookForFactoriesIn(packageName);
    }

    public static Map<Class, ServiceLocatorFactory> getFactories() {
        return internalServiceLocator.getFactories();
    }

    public static void reset() {
        changeInternalServiceLocator(new InternalServiceLocatorImpl());
    }
}
