package com.tomgpdev;

import com.tomgpdev.servicelocator.ServiceLocator;
import com.tomgpdev.servicelocator.ServiceLocatorFactory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class ServiceLocatorTest {

    @Test
    void checkWeCanInstantiateAllClassesFromFactories() {
        for (ServiceLocatorFactory value : ServiceLocator.getFactories().values()) {
            assertNotNull(value.get());
        }
    }
}
