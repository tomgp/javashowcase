package com.tomgpdev;

import com.tomgpdev.models.Car;
import com.tomgpdev.models.Tire;
import com.tomgpdev.models.impl.CarImpl;
import com.tomgpdev.servicelocator.InternalServiceLocator;
import com.tomgpdev.servicelocator.ServiceLocator;
import com.tomgpdev.servicelocator.ServiceLocatorFactory;
import com.tomgpdev.testfactories.CarTestFactory;
import com.tomgpdev.testfactories.TireTestFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;

class MainTest {
    private static final Log log = LogFactory.getLog(MainTest.class);

    private Main main;

    @BeforeEach
    void setUp() {
        main = new Main();

        ServiceLocator.reset();
    }

    @Test
    void generatingOurOwnFactoriesAndOnlyMockingTires() {
        final Tire mockedTire = Mockito.mock(Tire.class);
        Mockito.doAnswer(invocation -> {
            log.debug("Omg!!!! Omg!!!!");
            return null;
        }).when(mockedTire).roll();

        Map<Class, ServiceLocatorFactory> factories = new HashMap<>();

        factories.put(Car.class, new ServiceLocatorFactory<Car>() {
            @Override
            public Car get() {
                return new CarImpl();
            }

            @Override
            public Class<Car> classToRegister() {
                return Car.class;
            }
        });

        factories.put(Tire.class, new ServiceLocatorFactory<Tire>() {
            @Override
            public Tire get() {
                return mockedTire;
            }

            @Override
            public Class<Tire> classToRegister() {
                return Tire.class;
            }
        });

        ServiceLocator.setFactories(factories);

        main.letsDoSomething();

        Mockito.verify(mockedTire, Mockito.times(4)).roll();
    }

    @Test
    void changingServiceLocatorImplementation() {
        final Map<Class, Object> mockedClasses = new HashMap<>();

        InternalServiceLocator internalServiceLocator = new InternalServiceLocator() {
            @Override
            public <T> T get(Class<T> classToInstantiate) {
                if (!mockedClasses.containsKey(classToInstantiate)) {
                    mockedClasses.put(classToInstantiate, Mockito.mock(classToInstantiate));
                }

                return (T) mockedClasses.get(classToInstantiate);
            }

            @Override
            public void setFactories(Map<Class, ServiceLocatorFactory> factories) {

            }

            @Override
            public Map<Class, ServiceLocatorFactory> getFactories() {
                return null;
            }

            @Override
            public void lookForFactoriesIn(String packageName) {

            }
        };

        ServiceLocator.changeInternalServiceLocator(internalServiceLocator);

        Car mockedCar = ServiceLocator.get(Car.class);
        Tire mockedTire = ServiceLocator.get(Tire.class);

        Mockito.doAnswer(invocation -> {
            log.debug("Hey!!! Hey!!!!");
            return null;
        }).when(mockedCar).startEngine();

        main.letsDoSomething();

        Mockito.verify(mockedCar).startEngine();
        Mockito.verifyZeroInteractions(mockedTire);

    }

    @Test
    void changingWhereServiceLocatorLooksForFactories() {
        ServiceLocator.lookForFactoriesIn(getClass().getPackage().getName() + ".testfactories");

        Mockito.doAnswer(invocation -> {
            log.debug("Eiiiii!!!! Eiiiii!!");
            ServiceLocator.get(Tire.class).roll();
            return null;
        }).when(CarTestFactory.CAR_MOCK).startEngine();

        Mockito.doAnswer(invocation -> {
            log.debug("Tired!!!! Tired!!");
            return null;
        }).when(TireTestFactory.TIRE_MOCK).roll();

        main.letsDoSomething();

        Mockito.verify(CarTestFactory.CAR_MOCK).startEngine();
        Mockito.verify(TireTestFactory.TIRE_MOCK).roll();
    }
}
