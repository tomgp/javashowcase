package com.tomgpdev.testfactories;

import com.tomgpdev.models.Car;
import com.tomgpdev.models.Tire;
import com.tomgpdev.models.impl.CarImpl;
import com.tomgpdev.servicelocator.ServiceLocatorFactory;
import org.mockito.Mockito;

@SuppressWarnings("ALL")
public class CarTestFactory extends ServiceLocatorFactory<Car> {

    public static Car CAR_MOCK = Mockito.mock(Car.class);

    @Override
    public Car get() {
        return CAR_MOCK;
    }

    @Override
    public Class classToRegister() {
        return Car.class;
    }
}
