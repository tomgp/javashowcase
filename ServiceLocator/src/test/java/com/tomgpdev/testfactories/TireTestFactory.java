package com.tomgpdev.testfactories;

import com.tomgpdev.models.Tire;
import com.tomgpdev.models.impl.TireImpl;
import com.tomgpdev.servicelocator.ServiceLocatorFactory;
import org.mockito.Mockito;

@SuppressWarnings("unchecked")
public class TireTestFactory extends ServiceLocatorFactory<Tire> {

    public static Tire TIRE_MOCK = Mockito.mock(Tire.class);

    @Override
    public Tire get() {
        return TIRE_MOCK;
    }

    @Override
    public Class classToRegister() {
        return Tire.class;
    }
}
