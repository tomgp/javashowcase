Service Locator showcase
========================

I wanted to make my own Service Locator, to learn a bit about it. It's a pattern where all or almost all
classes avoid creating any object themselves. In this way, you can move out object generation &
their configuration to another specific places and have the opportunity to substitute 
implementations to whatever you need when you create tests, i.e. either mocking them or
provide an alternative implementation.

Not sure how well it is the one I provide here compared with other 
implementations out in the wild. 

To sum up, if you don't mind using a library, go for it. I'd recommend 
Dagger 2. If you want transparency and / or not adding another dependency to
your project though, go for your own implementation. Keep in mind that this adds
a critical weak point to your project if done incorrectly.

To run this project
-------------------
Apart from having Java installed in your system, you'll need Maven, then execute:

```
mvn exec:java test
```
